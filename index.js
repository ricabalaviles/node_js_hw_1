const express = require('express')
const morgan = require('morgan')
const app = express()
const PORT = 8080

app.use(morgan('tiny'))
app.use(express.json())

const {createFile} = require('./routes/createFile')
app.use(createFile)

const {getFiles} = require('./routes/getFiles')
app.use(getFiles)

const {getFile} = require('./routes/getFile')
app.use(getFile)

const {deleteFile} = require('./routes/deleteFile')
app.use(deleteFile)

const {editFile} = require('./routes/editFile')
app.use(editFile)

try{
  app.listen(PORT)  
} catch(err) {
    res.status(500).json({ "message": "Server error" })
    throw err
}

const credentials = []
