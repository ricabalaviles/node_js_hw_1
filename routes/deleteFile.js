const express = require('express')
const fs = require('fs')
const path = require('path')
const router = express.Router()

router.delete('/api/files/:filename', (req, res) => {
    const { filename } = req.params
    const filePath = 'api/files/' + filename

    // const filePath = path.resolve(filename)
    // console.log(filePath + '!!!')

    fs.unlink(filePath, err => {
        if (err) {
            return res.status(400).json({ "message": "file was not deleted" })
            
        } else {
            res.status(200).json({ "message": "file was deleted succsesfully!!!" })
        }
    })
})

module.exports = {
    deleteFile: router
}