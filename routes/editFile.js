const express = require('express')
const fs = require('fs')
const router = express.Router()

router.put('/api/files/:filename', (req, res) => {

    const { filename } = req.params
    const filePath = 'api/files/' + filename
    const filecontent = req.body.content

    fs.readFile(`${filePath}`, 'utf-8', function (err, data) {
        if (err) {

            return res.status(400).json({ "message": `Error: no file with' ${filename}' filename founded` })

        } else {

            const newContent = data + filecontent;
            const editedFileName = 'api/files/' + filename

            fs.writeFile(`${editedFileName}`, newContent, 'utf-8', function (err) {

                if (err) {
                    res.status(400).json({ "message": err })
                    throw err;

                } else {
                    res.status(200).json({
                        "message": "edited sucsessfuly",
                        "content": filecontent
                    })
                }
            });
        }


    });

    // fs.appendFile(filePath, filecontent, (err) => {
    //     if (err) {
    //         res.status(400).json({ "message": err })
    //         throw err;
    //     } else {
    //         res.status(200).json({
    //             "message": "edited sucsessfuly",
    //             "content": filecontent
    //         })
    //     }
    // })
})

module.exports = {
    editFile: router
}