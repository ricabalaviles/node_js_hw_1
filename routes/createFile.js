const express = require('express')
const fs = require('fs')
const router = express.Router()
const regExpFileExt = /([a-zA-Z0-9\s_\\.\-\(\):])+(.txt|.json|.js|.log|.yaml|.xml)$/i

router.post('/api/files', (req, res) => {

    const filename = 'api/files/' + req.body.filename
    const filecontent = req.body.content
    // const password = req.body.password

        if (regExpFileExt.test(filename)) {
           
            fs.writeFile(filename, filecontent, (err) => {
                if (err) {
                    res.status(400).json({ "message": "Please specify 'content' parameter" })
                    throw err;
                }

                res.status(200).json({ "message": 'File created successfully' })
            });
        } else {
            return res.status(400).json({ 
                "message": 'Please specify "content" parameter'
             })
        }

})


module.exports = {
    createFile: router
}