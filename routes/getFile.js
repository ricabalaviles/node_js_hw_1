const express = require('express')
const router = express.Router()
const fs = require('fs')
const path = require('path')


router.get('/api/files/:filename', (req, res) => {

    const { filename } = req.params

    const filePath = 'api/files/' + filename
    let uploadInfo

    fs.stat(filePath, (err, stats) => {
        if (err) {
            return res.status(400).json({ "message": `No file with '${filename}' filename found` })

        } else {
            uploadInfo = stats.birthtime
            
            fs.readFile(filePath, 'utf8', (err, data) => {
                if (err) {
                    res.status(400).json({ "message": `No file with '${filename}' filename found` })
                } else {
                    // const arr = filename.split('.')
                    // const fileExtension = arr[arr.length - 1]
                    const fileExtension = path.extname(filename).split('.')[1]

                    res.status(200).json({
                        "message": "Success",
                        "filename": filename,
                        "content": data,
                        "extension": fileExtension,
                        "uploadedDate": uploadInfo,
                    })
                }
            })
        }

    })


})

module.exports = {
    getFile: router
}