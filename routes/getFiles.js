const express = require('express')
const router = express.Router()
const fs = require('fs').promises
const path = require('path')

router.get('/api/files', (req, res) => {

        const filesPath = path.resolve('api/files')

        fs.readdir(filesPath, 'utf8')
            .then( data => {
                res.status(200).json({ 
                    "message": "Success", 
                    "files": data 
                })
            })
            .catch((err) => {
                res.status(400).json({ "message": "Client error" })
            })

        // fs.readdir(filesPath, (err, files) => {

        //     if (err) {
        //         res.status(400).json({ "message": "Client error" })
        //     }
        //     res.status(200).json({ 
        //         "message": "Success", 
        //         "files": files 
        //     })
        // });
})

module.exports = {
    getFiles: router
}